<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 23.11.2017
 * Time: 12:33
 */

namespace Terma;

use Monolog\Handler\FingersCrossedHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

require '../vendor/autoload.php';

$env = include_once '../env.php';

$logger = new Logger('terma_invoice');
$debugConfig = $env['debug_config'];
$stream = new StreamHandler($debugConfig['log_path'], $debugConfig['debug_level']);
$fingersCrossed = new FingersCrossedHandler($stream, $debugConfig['debug_level']);
$logger->pushHandler($fingersCrossed);

$success = null;
$filename = null;

if (isset($_POST["submit"])) {
  $generator = new Generator($env, $logger);

  if ($generator->verifyForm($_POST, $_FILES)) {
    //generate invoice description
    if($generator->generatePDF()) {
      $filename = $generator->getGeneratedFilename();
      //send email with file
      $success = $generator->sendMail();
      header('Location: /?s=' . $success . '&f=' . $filename);
    } else {
      handleError($logger, $generator);
    }
  } else {
    handleError($logger, $generator);
  }
}

function handleError(Logger $logger, Generator $generator) {
  $logger->addError(json_encode($generator->getErrors()));
  $fileError = $generator->getError('invoiceFile');
  if ($fileError) {
    header('Location: /?s=0&e=' . $fileError[1]);
  } else {
    header('Location: /?s=0');
  }
}
