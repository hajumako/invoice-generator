<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 12.12.2017
 * Time: 09:16
 */

if(!empty($_GET['file'])) {
  $env = include_once '../env.php';
  $absolutePath = $env['file_dirs']['generated'] . $_GET['file'];
  $pathParts = pathinfo($absolutePath);
  $fileName = $pathParts['basename'];
  $fileInfo = finfo_open(FILEINFO_MIME_TYPE);
  $fileType = finfo_file($fileInfo, $absolutePath);
  finfo_close($fileInfo);
  $fileSize = filesize($absolutePath);
  header('Content-Length: ' . $fileSize);
  header('Content-Type: ' . $fileType);
  header('Content-Disposition: attachment; filename=' . $fileName);
  flush();
  readfile($absolutePath);
  exit;
} else {
  die();
}
?>