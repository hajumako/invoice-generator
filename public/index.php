<?php

namespace Terma;

require 'generatePDF.php';
?>

<!doctype html>
<html class="no-js" lang="">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title><?php echo $env['title']; ?></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/manifest.json">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="theme-color" content="#ffffff">

  <link rel="stylesheet" href="css/main.css">
</head>
<body>

<?php if ($env['debug_config']['on']) : ?>
  <script src="js/testData.js"></script>
  <div class="button orange" onclick="fillTestData()">Wypełnij testowymi danymi</div>
<?php endif; ?>

<div class="wrapper">

  <div class="grid-x group">
    <div class="small-10 small-offset-1 medium-8 medium-offset-2">
      <h3 class="text-center">Generator opisu faktur</h3>
    </div>
    <div class="small-8 small-offset-2 medium-2 medium-offset-0 text-center">
      <a href="<?= $env['norm_link']; ?>" class="button">Link do normatywu</a>
    </div>
  </div>

  <?php if (isset($_GET['s'])): ?>
    <div class="msgContainer text-center">
      <?php if ($_GET['s']): ?>
        <h4 class="msg success">Opis faktury został wygenerowany i wysłany do Zespołu Księgowości</h4>
        <?php if (isset($_GET['f'])) : ?>
          <a href="download.php?file=<?= $_GET['f']; ?>" class="button">Pobierz PDF</a>
        <?php endif; ?>
      <?php else: ?>
        <?php if (isset($_GET['e'])): ?>
          <?php if ($_GET['e'] === '-1'): ?>
            <h4 class="msg error">Nieprawidłowy format pliku</h4>
          <?php elseif ($_GET['e'] === '1' || $_GET['e'] === '2'): ?>
            <h4 class="msg error">Zbyt duży plik</h4>
          <?php elseif ($_GET['e'] === '9'): ?>
            <h4 class="msg error">Plik po dekompresji jest zbyt duży</h4>
          <?php else: ?>
            <h4 class="msg error">Błąd zapisu pliku</h4>
          <?php endif; ?>
        <?php else: ?>
          <h4 class="msg error">Błąd przy generowaniu opisu do faktury</h4>
        <?php endif; ?>
      <?php endif; ?>
    </div>
  <?php endif; ?>

  <div id="formError" class="msgContainer text-center hide">
    <h4 class="msg error">Uzupełnij wszystkie pola</h4>
  </div>

  <form name="invoiceForm" action="" method="post" enctype="multipart/form-data">

    <label for="<?= $env['honeycomb']['name']; ?>" class="honeycomb">
      <input type="text" name="<?= $env['honeycomb']['name']; ?>" id="<?= $env['honeycomb']['name']; ?>">
    </label>

    <div class="grid-x grid-padding-x group">
      <label class="small-2 cell middle large-border-right no-bottom-margin">Faktura</label>
      <div class="shrink cell pad">
        <label for="invoiceFile" class="button no-bottom-margin"><span>Wybierz plik</span></label>
        <input type="file" name="invoiceFile" id="invoiceFile" class="show-for-sr" required>
      </div>
      <div class="auto cell pad">
        <span
            class="help-text">* Plik z fakturą powinien być w formacie <b>pdf</b>, <b>jpeg</b>, <b>jpg</b> lub <b>png</b>.</span>
        <span class="help-text">Maksymalny rozmiar to <b><?php echo UploadHandler::getUploadMaxSize(); ?></b></span>
      </div>
    </div>

    <div class="grid-x grid-padding-x group">
      <label class="medium-2 cell middle hide-for-small-only large-border-right no-bottom-margin">Typ faktury</label>
      <div class="small-12 medium-10 cell grid-x">
        <div class="small-12 medium-4 cell pad">
          <label for="terma" class="button white full-width no-bottom-margin tab-radio">
            <input type="radio" name="invoiceType" value="terma" id="terma" class="show-for-sr" required>
            <span class="text-center">
              <span class="tick">&#10004;</span>
              <span>TERMA</span>
            </span>
          </label>
        </div>
        <div class="small-12 medium-4 cell pad">
          <label for="mido" class="button white full-width no-bottom-margin tab-radio">
            <input type="radio" name="invoiceType" value="mido" id="mido" class="show-for-sr" required>
            <span class="text-center">
              <span class="tick">&#10004;</span>
              <span>MIDO</span>
            </span>
          </label>
        </div>
        <div class="small-12 medium-4 cell pad">
          <label for="fundacja" class="button white full-width no-bottom-margin tab-radio">
            <input type="radio" name="invoiceType" value="fundacja" id="fundacja" class="show-for-sr" required>
            <span class="text-center">
              <span class="tick">&#10004;</span>
              <span>FUNDACJA</span>
            </span>
          </label>
        </div>
      </div>
    </div>

    <div class="grid-x grid-padding-x group">
      <label class="medium-2 cell middle hide-for-small-only large-border-right no-bottom-margin">V dostawcy lub nazwa
        firmy</label>
      <div class="small-12 medium-4 cell pad">
        <input type="text" name="company" id="company" class="no-bottom-margin" required>
      </div>
    </div>

    <div class="grid-x grid-padding-x group">
      <label class="medium-2 cell middle hide-for-small-only large-border-right no-bottom-margin">Nr faktury</label>
      <div class="small-12 medium-4 cell pad">
        <input type="text" name="invoiceNo" id="invoiceNo" class="no-bottom-margin" required>
      </div>
    </div>

    <div class="grid-x grid-padding-x group">
      <label class="small-12 large-2 cell middle large-border-right no-bottom-margin">Opis</label>
      <div class="small-12 large-10 cell pad">
        <textarea type="textarea" name="description" id="description" class="no-bottom-margin" placeholder="opis"
                  rows="10" required></textarea>
      </div>
    </div>

    <div class="grid-x grid-padding-x group">
      <div class="small-2 small-offset-2 medium-offset-6 large-offset-7 cell">
        <label for="signature" class="middle text-right">Podpis</label>
      </div>
      <div class="small-8 medium-4 large-3 cell">
        <input type="text" name="signature" id="signature" required>
      </div>
    </div>

    <input type="submit" value="Generuj" name="submit" class="button">

  </form>
</div>

<div id="sending" class="modalWrapper hide">
  <div class="modal">
    <div>
      <img src="img/ripple.gif" alt="">
      <h2>Generowanie</h2>
    </div>
  </div>
</div>

<div class="modalWrapper">
  <div class="modal blue wide">
    <div>
      <h2>Nowa wersja generatora</h2>
      <p><a href="<?= $env['new_version']; ?>"><?= $env['new_version']; ?></a></p>
      <div class="grid-x">
        <a href="<?= $env['new_version']; ?>" class="small-4 small-offset-1 cell button red">Zobacz</a>
        <div class="small-4 small-offset-2 cell button close-modal">Zamknij</div>
      </div>
    </div>
  </div>
</div>

<script src="js/main.js"></script>

<!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
<script>
  window.ga = function () {
    ga.q.push(arguments);
  };
  ga.q = [];
  ga.l = +new Date;
  ga('create', '<?php echo $env['analytics']; ?>', 'auto');
  ga('send', 'pageview');
</script>

<script src="https://www.google-analytics.com/analytics.js" async defer></script>

</body>
<?php if ($env['debug_config']['on']) : ?>
  <script>
    console.log('title:', '<?php echo $env['title']; ?>');
    console.log('branch:', '<?php echo $env['branch']; ?>');
    console.log('hash:', '<?php echo $env['hash']; ?>');
  </script>
<?php endif; ?>

</html>
