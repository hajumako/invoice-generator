<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 30.11.2017
 * Time: 11:10
 */

namespace Terma;

class PdfTemplate
{
  const TMPL1 = 't1';

  public static function getStyles(string $template = self::TMPL1): string
  {
    if ($template == self::TMPL1) {
      return self::styles1();
    } else {
      return 'styles not found';
    }
  }

  public static function render(array $data, string $template = self::TMPL1): string
  {
    if ($template == self::TMPL1) {
      return self::template1($data);
    } else {
      return 'template not found';
    }
  }

  protected static function styles1()
  {
    return file_get_contents(__DIR__ . '/../../public/css/pdf_style1.css');
  }

  protected static function template1($d)
  {
    if (array_key_exists('description', $d)) {
      $description = '
      <div class="description">
        <div>' . $d['description'] . '</div>
      </div>';
      //$description = str_replace(" ", '&nbsp;', $description);
      $description = str_replace("\t", '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', $description);
      $description = nl2br($description);
    } else {
      $description = '<div class="description">Brak</div>';
    }

    $costComponents = '';
    if (array_key_exists('costComponents', $d)) {
      $costComponents = '<table class="costComponents">';
      foreach ($d['costComponents'] as $i => $costComponent) {
        $costComponents .= '
      <tr>
        <td class="no" valign="top">' . ($i + 1) . '.</td>
        <td class="componentDescription" valign="top">' . $costComponent['componentDescription'] . '</td>
        <td class="componentAmount amount" valign="top">' . $costComponent['componentAmount'] . '</td>
        <td class="componentCurrency currency" valign="top">' . $costComponent['componentCurrency'] . '</td>
      </tr>';
      }
      if (array_key_exists('componentsTotal', $d)) {
        $costComponents .= '
      <tr class="componentsTotal">
        <td colspan="2" valign="top">Razem:</td>
        <td valign="top">' . $d['componentsTotal'] . '</td>
        <td class="currency" valign="top">PLN</td>
      </tr>';
      }
      $costComponents .= '</table>';
    }

    $deliveryOrders = '<table class="deliveryOrders">';
    if (array_key_exists('deliveryOrders', $d)) {
      foreach ($d['deliveryOrders'] as $i => $deliveryOrder) {
        $deliveryOrders .= '
      <tr>
        <td class="orderNo" valign="top">' . $deliveryOrder['orderNo'] . '</td>
        <td class="orderDescription" valign="top">' . $deliveryOrder['orderDescription'] . '</td>
      </tr>';
      }
    }
    $deliveryOrders .= '</table>';

    $dimensions = '<table class="dimensions">';
    if (array_key_exists('financialDims', $d)) {
      foreach ($d['financialDims'] as $i => $financialDim) {
        $dimensions .= '
      <tr>
        <td class="code" valign="top">' . $financialDim['Dział'] . '-' .
          $financialDim['Jednostka_biznesowa'] . '-' .
          $financialDim['MPK'] . '-' .
          $financialDim['Region'] .
          ($financialDim['financialDimTransportInvoiceNo'] ? '-' . $financialDim['financialDimTransportInvoiceNo'] : '') . '
        </td>
        <td class="financialDimTransportCost" valign="top">' . $financialDim['financialDimTransportCost'] . '</td>
        <td class="financialDimAmount amount" valign="top">' . $financialDim['financialDimAmount'] . '</td>
        <td class="financialDimCurrency currency" valign="top">' . $financialDim['financialDimCurrency'] . '</td>
      </tr>';
      }
    }
    if (array_key_exists('deliveryDims', $d)) {
      foreach ($d['deliveryDims'] as $i => $deliveryDim) {
        $dimensions .= '
      <tr>
        <td class="deliveryDimOrderNo" valign="top">' . $deliveryDim['deliveryDimOrderNo'] . '</td>
        <td class="deliveryDimAmount amount" colspan="2" valign="top">' . $deliveryDim['deliveryDimAmount'] . '</td>
        <td class="deliveryDimCurrency currency" valign="top">' . $deliveryDim['deliveryDimCurrency'] . '</td>
      </tr>';
      }
    }
    if (array_key_exists('dimensionsTotal', $d)) {
      $dimensions .= '
      <tr class="dimensionsTotal">
        <td colspan="2" valign="top">Razem:</td>
        <td valign="top">' . $d['dimensionsTotal'] . '</td>
        <td class="currency" valign="top">PLN</td>
      </tr>';
    }
    $dimensions .= '</table>';

    if (array_key_exists('signature', $d)) {
      $signature = '<div class="signature">' . $d['signature'] . '</div>';
    } else {
      $signature = '<div class="signature">Brak</div>';
    }

    return '<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
</head>
<!-- END HEAD -->

<body>
    <div class="wrapper">'
      . $description
      . $costComponents
      . $deliveryOrders
      . $dimensions
      . $signature
      . '</div >
    </body>

</html>';
  }
}