<?php

namespace Terma\Guesser;

use \RuntimeException;


class RegexGuesser
{
  public function guess($file)
  {
    $version = $this->guessVersion($file);

    if ($version === null)
      throw new RuntimeException("Can't guess version. The file '{$file}' is a PDF file?");

    return $version;
  }

  protected function guessVersion($filename)
  {
    $fp = @fopen($filename, 'rb');

    if (!$fp) {
      return 0;
    }

    /* Reset file pointer to the start */
    fseek($fp, 0);

    /* Read 1024 bytes from the start of the PDF */
    preg_match('/%PDF-(\d\.\d)/', fread($fp, 1024), $match);

    fclose($fp);

    if (isset($match[1]))
      return $match[1];

    return null;
  }
}
