<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 29.03.2018
 * Time: 12:55
 */

namespace Terma\Converter;


use Symfony\Component\Process\Process;

class ProcessWindows extends Process
{
  protected $process;
  protected $contents;

  public function runWindows($input = null): int
  {
    if ('\\' === DIRECTORY_SEPARATOR) {
      $descriptorspec = array(
        0 => array("pipe", "r"),  // stdin is a pipe that the child will read from
        1 => array("pipe", "w"),  // stdout is a pipe that the child will write to
        2 => array("file", "../logs/proc_error.txt", "a") // stderr is a file to write to
      );

      $commandline = parent::getCommandLine();
      $process = proc_open($commandline, $descriptorspec, $pipes, null, null, array('bypass_shell' => true));

      if (is_resource($process)) {
        if ($input) {
          // writeable handle connected to child stdin
          fwrite($pipes[0], $input);
          fclose($pipes[0]);
        }

        // readable handle connected to child stdout
        $this->contents = stream_get_contents($pipes[1]);
        fclose($pipes[1]);

        return proc_close($process);
      }

      return -1;
    } else {

      return parent::run();
    }
  }

  public function getContents()
  {
    return $this->contents;
  }
}