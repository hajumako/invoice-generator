<?php

namespace Terma\Converter;

use Symfony\Component\Filesystem\Filesystem;


class ImageMagickRunner
{
  protected $magickPath;
  protected $fs;
  protected $tmp;

  /**
   * ImageMagickRunner constructor.
   * @param string $magickPath
   * @param Filesystem $fs
   * @param null $tmp
   */
  public function __construct(string $magickPath, Filesystem $fs, $tmp = null)
  {
    $this->magickPath = $magickPath;
    $this->fs = $fs;
    $this->tmp = $tmp ?: sys_get_temp_dir();
  }

  /**
   * @param string $type
   * @param string $inputFile
   * @param string|null $outputDir
   *
   * @return string
   */
  public function convert(string $type, string $inputFile, string $outputDir = null)
  {
    $pinfo = pathinfo($inputFile);
    $outputDir = $outputDir ?: $pinfo['dirname'] . DIRECTORY_SEPARATOR;
    $outputPath = $outputDir . preg_replace('/(\.[a-zA-z]+)/', '.' . $type, $pinfo['basename']);

    $command = new ImageMagickConverterCommand($this->magickPath);
    $result = $command->run($type, $inputFile, $outputPath);

    if($result === -1) {
      return null;
    }

    return $outputPath;
  }
}