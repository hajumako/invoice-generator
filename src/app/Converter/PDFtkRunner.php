<?php

namespace Terma\Converter;

use Symfony\Component\Filesystem\Filesystem;

class PDFtkRunner
{
  protected $pdftkPath;
  protected $fs;
  protected $tmp;

  /**
   * PDFtkRunner constructor.
   * @param string $pdftkPath
   * @param Filesystem $fs
   * @param null $tmp
   */
  public function __construct(string $pdftkPath, Filesystem $fs, $tmp = null)
  {
    $this->pdftkPath = $pdftkPath;
    $this->fs = $fs;
    $this->tmp = $tmp ?: sys_get_temp_dir();
  }

  /**
   * @param string $newVersion
   * @param string $inputFile
   * @param string|null $outputDir
   *
   * @return string
   */
  public function compress(string $inputFile, string $outputDir = null)
  {
    $command = new PDFtkCompressCommand($this->pdftkPath);
    $pinfo = pathinfo($inputFile);
    $outputDir = $outputDir ?: $pinfo['dirname'] . DIRECTORY_SEPARATOR;
    $outputPath = $outputDir . $pinfo['basename'];

    if($inputFile !== $outputPath) {
      $result = $command->run($inputFile, $outputPath);
    } else {
      $tmpFile = $this->generateAbsolutePathOfTmpFile();
      $result = $command->run($inputFile, $tmpFile);
      if (!$this->fs->exists($tmpFile)) {
        throw new \RuntimeException("The generated file '{$tmpFile}' was not found.");
      }
      $this->fs->copy($tmpFile, $outputPath, true);
    }

    if($result === -1) {
      return null;
    }

    return $outputPath;
  }

  /**
   * Generates a unique absolute path for tmp file.
   * @return string absolute path
   */
  protected function generateAbsolutePathOfTmpFile()
  {
    return $this->tmp . '/' . uniqid('pdftk_runner_') . '.pdf';
  }
}