<?php

namespace Terma\Converter;


class QpdfDecryptCommand
{
  protected $baseCommand;

  const PARAMS = ' -password="" -decrypt %s %s';

  /**
   * ImageMagickConverterCommand constructor.
   * @param string $qpdf
   */
  public function __construct(string $qpdf)
  {
    $this->baseCommand = $qpdf . self::PARAMS;
  }

  /**
   * @param string $inputPath
   * @param string $outputPath
   *
   * @return int|null
   */
  public function run(string $inputPath, string $outputPath)
  {
    $command = sprintf($this->baseCommand, escapeshellarg($inputPath), escapeshellarg($outputPath));
    $process = new ProcessWindows($command);

    return $process->runWindows();
  }
}
