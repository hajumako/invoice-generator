<?php

namespace Terma\Converter;


class PDFtkCompressCommand
{
  protected $baseCommand;

  const PARAMS = ' %s output %s compress';

  /**
   * PDFtkCompressCommand constructor.
   * @param string $pdftkPath
   */
  public function __construct(string $pdftkPath)
  {
    $this->baseCommand = $pdftkPath . self::PARAMS;
  }

  /**
   * @param string $inputPath
   * @param string $outputPath
   *
   * @return int|null
   */
  public function run(string $inputPath, string $outputPath)
  {
    $command = sprintf($this->baseCommand, escapeshellarg($inputPath), escapeshellarg($outputPath));
    $process = new ProcessWindows($command);

    return $process->runWindows();
  }
}
