<?php

namespace Terma\Converter;


class GhostscriptConverterCommand
{
  protected $baseCommand;

  const PARAMS = ' -sDEVICE=pdfwrite -dCompatibilityLevel=%s -dPDFSETTINGS=/screen -dNOPAUSE -dQUIET -dBATCH -dColorConversionStrategy=/LeaveColorUnchanged -dEncodeColorImages=false -dEncodeGrayImages=false -dEncodeMonoImages=false -dDownsampleMonoImages=false -dDownsampleGrayImages=false -dDownsampleColorImages=false -dAutoFilterColorImages=false -dAutoFilterGrayImages=false -dColorImageFilter=/FlateEncode -dGrayImageFilter=/FlateEncode  -sOutputFile=%s %s';

  /**
   * GhostscriptConverterCommand constructor.
   * @param string $gsPath
   */
  public function __construct(string $gsPath)
  {
    $this->baseCommand = $gsPath . self::PARAMS;
  }

  /**
   * @param string $inputPath
   * @param string $outputPath
   * @param string $newVersion
   *
   * @return int|null
   */
  public function run(string $newVersion, string $inputPath, string $outputPath)
  {
    $command = sprintf($this->baseCommand, $newVersion, escapeshellarg($outputPath), escapeshellarg($inputPath));
    $process = new ProcessWindows($command);

    return $process->runWindows();
  }
}
