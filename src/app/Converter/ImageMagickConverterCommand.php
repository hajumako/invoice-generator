<?php

namespace Terma\Converter;


class ImageMagickConverterCommand
{
  protected $baseCommand;

  const PARAMS = 'convert.exe %s %s';
  const PDF = 'pdf';

  /**
   * ImageMagickConverterCommand constructor.
   * @param string $magickPath
   */
  public function __construct(string $magickPath)
  {
    $this->baseCommand = $magickPath . self::PARAMS;
  }

  /**
   * @param string $type
   * @param string $inputPath
   * @param string $outputPath
   *
   * @return int|null
   */
  public function run(string $type, string $inputPath, string $outputPath)
  {
    $command = sprintf($this->baseCommand, escapeshellarg($inputPath), escapeshellarg($outputPath));
    $process = new ProcessWindows($command);

    return $process->runWindows();
  }
}
