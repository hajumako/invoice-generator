<?php

namespace Terma\Converter;

use Symfony\Component\Filesystem\Filesystem;


class QpdfRunner
{
  protected $qpdf;
  protected $fs;
  protected $tmp;

  /**
   * ImageMagickRunner constructor.
   * @param string $qpdfPath
   * @param Filesystem $fs
   * @param null $tmp
   */
  public function __construct(string $qpdfPath, Filesystem $fs, $tmp = null)
  {
    $this->qpdf = $qpdfPath;
    $this->fs = $fs;
    $this->tmp = $tmp ?: sys_get_temp_dir();
  }

  /**
   * @param string $inputFile
   * @param string|null $outputDir
   *
   * @return string
   */
  public function decrypt(string $inputFile, string $outputDir = null)
  {
    $command = new QpdfDecryptCommand($this->qpdf);
    $pinfo = pathinfo($inputFile);
    $outputDir = $outputDir ?: $pinfo['dirname'] . DIRECTORY_SEPARATOR;
    $outputPath = $outputDir . $pinfo['basename'];
    if ($inputFile !== $outputPath) {
      $result = $command->run($inputFile, $outputPath);
    } else {
      $tmpFile = $this->generateAbsolutePathOfTmpFile();
      $result = $command->run($inputFile, $tmpFile);
      if (!$this->fs->exists($tmpFile)) {
        throw new \RuntimeException("The generated file '{$tmpFile}' was not found.");
      }
      $this->fs->copy($tmpFile, $outputPath, true);
    }

    if($result === -1) {
      return null;
    }

    return $outputPath;
  }

  /**
   * Generates a unique absolute path for tmp file.
   * @return string absolute path
   */
  protected function generateAbsolutePathOfTmpFile()
  {
    return $this->tmp . '/' . uniqid('qpdf_runner_') . '.pdf';
  }
}