<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 23.11.2017
 * Time: 09:41
 */

namespace Terma;

use DateTime;
use DateTimeZone;
use finfo;
use Monolog\Logger;
use Mpdf\Mpdf;
use Mpdf\Output\Destination;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use Symfony\Component\Filesystem\Filesystem;
use Terma\Converter\GhostscriptRunner;
use Terma\Converter\ImageMagickConverterCommand;
use Terma\Converter\ImageMagickRunner;
use Terma\Converter\PDFtkRunner;
use Terma\Converter\QpdfRunner;
use Terma\Guesser\RegexGuesser;

class Generator
{
  protected $errors = array();
  protected $env;
  protected $logger;
  protected $mimeType;
  protected $uploadedFilename;
  protected $convertedFilename;
  protected $generatedFilename;
  protected $sanitizedData = array();
  protected $html;
  protected $css;
  protected $suffix;

  public function __construct(array $env, Logger $logger)
  {
    $this->logger = $logger;
    $this->env = $env;
    $date = new DateTime();
    $this->suffix = '___' . $date->format('Ymd_His');
  }

  public function getErrors()
  {
    return $this->errors;
  }

  public function getError(string $name)
  {
    if (array_key_exists($name, $this->errors)) {
      return $this->errors[$name];
    }

    return null;
  }

  public function verifyForm(array $datas, array $files): bool
  {
    $this->log([$datas, $files]);
    $valid = true;
    foreach ($files as $i => $file) {
      if ($file['error'] > 0) {
        if ($file['error'] === 1 || $file['error'] === 2) {
          $this->errors['invoiceFile'] = ['Zbyt duży plik', $file['error']];
        } else {
          $this->errors['invoiceFile'] = ['Błąd zapisu pliku', $file['error']];
        }
        $valid = false;
      } elseif ($this->checkMimeType($file['tmp_name'])) {
        $nameArray = explode('.', $file['name']);
        if (count($nameArray) === 1) {
          $fileName = $nameArray[0];
          $extension = null;
        } else {
          $fileName = '';
          $extension = $nameArray[count($nameArray) - 1];
          foreach ($nameArray as $j => $v) {
            if ($j < count($nameArray) - 1) {
              $fileName .= $v . '.';
            }
          }
          $fileName = trim($fileName, ".");
        }
        $outPath = $this->env['file_dirs']['uploaded'] . $fileName . $this->suffix . ($extension ? '.' . $extension : '');
        if (move_uploaded_file($file['tmp_name'], $outPath)) {
          $this->uploadedFilename = $outPath;
        }
      } else {
        $this->errors['invoiceFile'] = ['Nieprawidłowy format pliku', -1];
        $valid = false;
      }
    }

    if (!empty($datas['invoiceType'])) {
      $this->sanitizedData['invoiceType'] = htmlspecialchars($datas['invoiceType']);
    } else {
      $this->emptyFieldError('invoiceType');
      $valid = false;
    }

    if (!empty($datas['company'])) {
      $this->sanitizedData['company'] = htmlspecialchars($datas['company']);
    } else {
      $this->emptyFieldError('company');
      $valid = false;
    }

    if (!empty($datas['invoiceNo'])) {
      $this->sanitizedData['invoiceNo'] = htmlspecialchars($datas['invoiceNo']);
    } else {
      $this->emptyFieldError('invoiceNo');
      $valid = false;
    }

    //description
    if (!empty($datas['description'])) {
      $this->sanitizedData['description'] = htmlspecialchars($datas['description']);
    } else {
      $this->emptyFieldError('description');
      $valid = false;
    }

    if (!empty($datas['signature'])) {
      $this->sanitizedData['signature'] = htmlspecialchars($datas['signature']);
    } else {
      $this->emptyFieldError('signature');
      $valid = false;
    }

    //check if honeycomb is empty
    if ($datas[$this->env['honeycomb']['name']] !== '') {
      $this->errors['honeycomb'] = 'Pole powinno zostać puste';
      $valid = false;
    }

    return $valid;
  }

  protected function checkMimeType(string $filename): bool
  {
    if (!empty($filename)) {
      $finfo = new finfo(FILEINFO_MIME_TYPE);
      $this->mimeType = $finfo->file($filename);
      if (in_array($this->mimeType, $this->env['allowed_mime_types'])) {

        return true;
      }
    }

    return false;
  }

  /**
   * @param bool $withVersionChange
   *
   * @return null|string
   */
  public function generatePDF()
  {
    $this->convertedFilename = $this->convertToPdf($this->uploadedFilename);
    $this->convertedFilename = $this->decryptPdf($this->convertedFilename);
    $this->convertedFilename = $this->recompressPdf($this->convertedFilename);

    if ($this->convertedFilename && file_exists($this->convertedFilename)) {
      if (filesize($this->convertedFilename) > UploadHandler::getParsedUploadMaxSize()) {
        $this->errors['invoiceFile'] = ['Plik po dekompresji jest zbyt duży', 9];

        return false;
      }

      $mpdf = new mPDF();
      $mpdf->SetImportUse();

      $pageCount = $mpdf->SetSourceFile($this->convertedFilename);

      for ($i = 1; $i <= $pageCount; $i++) {
        $tpl = $mpdf->ImportPage($i);
        $size = $mpdf->getTemplateSize($tpl);
        $mpdf->AddPageByArray([
          'orientation' => $size['w'] < $size['h'] ? 'P' : 'L',
          'sheet-size' => [
            min($size['h'], $size['w']),
            max($size['h'], $size['w'])
          ]
        ]);
        $mpdf->UseTemplate($tpl, null, null, 0, 0);
      }
      $mpdf->AddPageByArray([
        'sheet-size' => 'A4'
      ]);
      $mpdf->defaultfooterline = 0;

      $this->css = PdfTemplate::getStyles(PdfTemplate::TMPL1);
      $this->html = PdfTemplate::render($this->sanitizedData, PdfTemplate::TMPL1);
      $mpdf->WriteHTML($this->css, 1);
      $mpdf->WriteHTML($this->html, 2);
      $filename = $this->sanitizeFilename($this->sanitizedData['company'] . '_' . $this->sanitizedData['invoiceNo']);
      $this->generatedFilename = $this->env['file_dirs']['generated'] . $filename . '_opis' . $this->suffix . '.pdf';

      $mpdf->Output($this->generatedFilename, Destination::FILE);

      return true;
    }

    return null;
  }

  /**
   * @return string
   */
  public function getGeneratedFilename(): string
  {
    return basename($this->generatedFilename);
  }

  /**
   * @return bool
   */
  public function sendMail(): bool
  {
    $mail = $this->loadMailerConfig();
    $mail->setFrom($this->env['mail']['from'], $this->env['mail']['name']);
    if ($this->env['debug_config']['on']) {
      $mail->SMTPDebug = $this->env['debug_config']['mailer_debug_level'];
      $mail->Debugoutput = $this->logger;
      foreach ($this->env['debug_config']['send_mail'] as $addr) {
        $mail->addAddress($addr);
      }
      if (array_key_exists($this->sanitizedData['invoiceType'], $this->env['mail']['to'])) {
        foreach ($this->env['mail']['to'][$this->sanitizedData['invoiceType']] as $addr) {
          $mail->addAddress($addr);
        }
      }
    }
    $mail->Subject = $this->env['mail']['subj'] . $this->sanitizedData['company'] . ' ' . $this->sanitizedData['invoiceNo'];
    $mail->MsgHTML('<p style="font-size: 11pt; font-family: Calibri">Wygenerowano opis dla faktury</p>' . $this->html);

    if (file_exists($this->generatedFilename)) {
      //invoice with description
      $mail->addStringAttachment(file_get_contents($this->generatedFilename), basename($this->removeSuffix($this->generatedFilename)));
    }
    if (file_exists($this->uploadedFilename)) {
      //original, uploaded file
      $mail->addStringAttachment(file_get_contents($this->uploadedFilename), basename($this->removeSuffix($this->uploadedFilename)));
    }

    return $mail->send();
  }

  protected function convertToPdf($file)
  {
    if ($file && in_array($this->mimeType, $this->env['allowed_image_types'])) {
      if (!empty($this->env['magick_path'])) {
        $imageMagickRunner = new ImageMagickRunner($this->env['magick_path'], new Filesystem());
        $file = $imageMagickRunner->convert(ImageMagickConverterCommand::PDF, $file, $this->env['file_dirs']['converted']);
      } else {
        throw new Exception('image magick path not set');
      }
    }

    return $file;
  }

  protected function convertPdfVersion($file)
  {
    $guesser = new RegexGuesser();
    //convert pdf to version 1.4 if greater than 1.4
    //mpdf throws error for compressed pdfs (used in versions >=1.5)
    //https://stackoverflow.com/questions/12154190/fpdf-error-this-document-testcopy-pdf-probably-uses-a-compression-technique-w
    if ($file && (double)$guesser->guess($file) > 1.4) {
      if (!empty($this->env['gs_path'])) {
        $ghostscriptRunner = new GhostscriptRunner($this->env['gs_path'], new Filesystem());
        $file = $ghostscriptRunner->convert('1.4', $file, $this->env['file_dirs']['converted']);
      } else {
        throw new Exception('ghost script path not set');
      }
    }

    return $file;
  }

  protected function decryptPdf($file)
  {
    if ($file) {
      if (!empty($this->env['qpdf_path'])) {
        $qpdfrunner = new QpdfRunner($this->env['qpdf_path'], new Filesystem());
        $file = $qpdfrunner->decrypt($file, $this->env['file_dirs']['converted']);
      } else {
        throw new Exception('qpdf path not set');
      }
    }

    return $file;
  }

  protected function recompressPdf($file)
  {
    if ($file) {
      if (!empty($this->env['pdftk_path'])) {
        $pdftkrunner = new PDFtkRunner($this->env['pdftk_path'], new Filesystem());
        $file = $pdftkrunner->compress($file, $this->env['file_dirs']['converted']);
      } else {
        throw new Exception('PDFtk path not set');
      }
    }

    return $file;
  }

  protected function log(array $datas)
  {
    if ($this->env['debug_config']['log_forms']) {
      $d = new DateTime();
      $logMsg = $d->setTimezone(new DateTimeZone('Europe/Warsaw'))->format('Y-m-d H:i:s T') .
        "\r\nREMOTE_ADDR:\t" . (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '') .
        "\r\nHTTP_X_FORWARDED_FOR:\t" . (isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : '') .
        "\r\n";
      foreach ($datas as $data) {
        ob_start();
        var_dump($data);
        $logMsg .= ob_get_clean() . "\r\n";
      }
      $this->logger->addInfo($logMsg);
    }
  }

  protected function loadMailerConfig(): PHPMailer
  {
    $mailConfig = $this->env['mail'];
    if (!empty($mailConfig)) {
      $mail = new PHPMailer();
      $mail->isSMTP();
      $mail->isHTML(true);
      $mail->CharSet = 'UTF-8';
      $mail->Host = $mailConfig['host'];
      $mail->Port = $mailConfig['port'];
      $mail->SMTPSecure = $mailConfig['secr'];
      $mail->SMTPAuth = $mailConfig['auth'];
      $mail->Username = $mailConfig['user'];
      $mail->Password = $mailConfig['pswd'];
      $mail->Timeout = $mailConfig['timeout'];

      //fix for incorrect SSL config on server
      //https://github.com/PHPMailer/PHPMailer/wiki/Troubleshooting#php-56-certificate-verification-failure
      $mail->SMTPOptions = array(
        'ssl' => array(
          'verify_peer' => false,
          'verify_peer_name' => false,
          'allow_self_signed' => true
        )
      );
      return $mail;
    } else {
      throw new Exception('missing mail config');
    }
  }

  protected function emptyFieldError($fieldName)
  {
    $this->errors[$fieldName] = 'Pole nie może być puste';
  }

  protected function removeSuffix(string $path)
  {
    return str_replace($this->suffix, '', $path);
  }

  protected function sanitizeFilename(string $filename)
  {
    // Remove anything which isn't a word, whitespace, number
    // or any of the following caracters -_~,;[]().
    $filename = mb_ereg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $filename);
    // Remove any runs of periods

    return mb_ereg_replace("([\.]{2,})", '', $filename);
  }
}