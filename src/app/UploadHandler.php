<?php
/**
 * Created by PhpStorm.
 * User: KucKo
 * Date: 06.04.2018
 * Time: 12:10
 */

namespace Terma;


class UploadHandler
{
  public function __construct()
  {
  }

  public static function getUploadMaxSize(): string
  {
    $max_size = -1;
    $ret = '';
    if ($max_size < 0) {
      // Start with post_max_size.
      $post_max_size_val = ini_get('post_max_size');
      $post_max_size = self::parseSize($post_max_size_val);
      if ($post_max_size > 0) {
        $max_size = $post_max_size;
        $ret = $post_max_size_val;
      }

      // If upload_max_size is less, then reduce. Except if upload_max_size is
      // zero, which indicates no limit.
      $upload_max_val = ini_get('upload_max_filesize');
      $upload_max = self::parseSize($upload_max_val);
      if ($upload_max > 0 && $upload_max < $max_size) {
        $max_size = $upload_max;
        $ret = $upload_max_val;
      }
    }

    return $ret . 'B';
  }

  public static function getParsedUploadMaxSize(): float
  {
    return self::parseSize(self::getUploadMaxSize());
  }

  protected static function parseSize(string $size): float
  {
    $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
    $size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
    if ($unit) {
      // Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
      return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
    } else {
      return round($size);
    }
  }
}
