(function () {
  // Attach the change event listener to change the label of all input[type=file] elements
  var els = document.querySelectorAll('input[type=file]'),
    i;
  for (i = 0; i < els.length; i++) {
    els[i].addEventListener('change', function () {
      var label = this.previousElementSibling;
      label.innerHTML = this.files[0].name;
    });
  }
})();

var $formError = document.getElementById('formError');

function addClass (el, name) {
  var arr = el.className.split(' ');
  if (arr.indexOf(name) === -1) {
    el.className += ' ' + name;
  }
}

[].forEach.call(
  document.getElementsByClassName('close-modal'),
  function (el) {
    el.addEventListener('click', function (e) {
      var modalWrapper = e.target.closest('.modalWrapper');
      if(modalWrapper) {
        addClass(modalWrapper, 'hide');
      }
    });
  }
);

document.forms.invoiceForm.addEventListener('submit', function (e) {
  if (
    !document.getElementById('invoiceFile').files.length ||
    (
      !document.getElementById('terma').checked &&
      !document.getElementById('mido').checked &&
      !document.getElementById('fundacja').checked
    ) ||
    document.getElementById('company').value === '' ||
    document.getElementById('invoiceNo').value === '' ||
    document.getElementById('description').value === '' ||
    document.getElementById('signature').value === '') {
    $formError.className = $formError.className.replace(' hide', '');
    e.preventDefault();
    return false;
  }
  var $sending = document.getElementById('sending');
  $sending.className = $sending.className.replace(' hide', '');
});

[].forEach.call(
  document.getElementsByTagName('input'),
  function (el) {
    el.addEventListener('focus', function (e) {
      addClass($formError, 'hide');
    });
  }
);

[].forEach.call(
  document.getElementsByTagName('textarea'),
  function (el) {
    el.addEventListener('focus', function (e) {
      addClass($formError, 'hide');
    });

    el.onkeydown = function (e) {
      if (e.keyCode === 9 || e.which === 9) {
        e.preventDefault();
        var s = this.selectionStart;
        this.value = this.value.substring(0, this.selectionStart) + '\t' + this.value.substring(this.selectionEnd);
        this.selectionEnd = s + 1;
      }
    };
  }
);
