window.fillTestData = function () {
  document.getElementById('terma').checked = true;
  document.forms.invoiceForm.elements['company'].value = 'Firma ABC';
  document.forms.invoiceForm.elements['invoiceNo'].value = "2018/03/04/!@#$%^&*()_-+={[}]|\:;\"\'<,>.?/2000";
  document.forms.invoiceForm.elements['description'].value =
    "Kółka do platformy transportowej koszy do myjki ERA-zmodyfikowanie platformy.\n\n" +
    "ZD/17/083338 – Wiertła HSS Co8 na wszystkie obszary produkcyjne\n\n" +
    "501-101-GXX-RE00\t\t  90,00 PLN\n" +
    "ZD/17/083338\t\t\t\t100,00 PLN\n" +
    "\t\t\t\tRazem:\t190,00 PLN\n";
  document.forms.invoiceForm.elements['signature'].value = 'Jan Kowalski';
};
