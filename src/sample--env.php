<?php

return [
  'root_dir' => __DIR__,
  'pdftk_path' => '',//path to pdftk.exe
  'magick_path' => '', //path to imagemagick folder
  'gs_path' => '', //path to gswin64c.exe
  'qpdf_path' => '', //path to qpdf.exe
  'norm_link' => '', //link to invoice description instruction
  'new_version' => '', //link to new version of generator
  'mail' => [
    'to' => [
      'fundacja' => [],
      'mido' => [],
      'terma' => []
    ],
    'from' => '',
    'name' => '',
    'subj' => '',
    'host' => '',
    'port' => '', //465 for ssl
    'secr' => '', //ssl or tls
    'auth' => '', //boolean
    'user' => '',
    'pswd' => '',
    'timeout' => 0 //int
  ],
  'fields' => [
    [
      'name' => '',
      'label' => '',
      'type' => '',
      'required' => '', //boolean
      'honeycomb' => '' //boolean
    ],
  ],
  'allowed_mime_types' => [
    //accepted file types
  ],
  'allowed_image_types' => [
    //accepted image types
  ],
  'file_dirs' => [
    'uploaded' => '',//i.e. __DIR__ . '/invoices/uploaded/',
    'generated' => ''//i.e. __DIR__ . '/invoices/generated/',
  ],
  'debug_config' => [
    'on' => '', //boolean
    'debug_level' => 100, //levels described in \vendor\monolog\monolog\src\Monolog\Logger.php
    'log_forms' => '', //boolean; always log submitted forms to log file
    'log_path' => __DIR__ . '/logs/app.log',
    'send_mail' => [
      'konrad.kucharski@termagroup.pl'
    ],
    'mailer_debug_level' => 4
  ],
  //do not edit below, handled by gulp
  'title' => '{{TITLE}}',
  'app_url' => '{{APP_URL}}',
  'branch' => '{{BRANCH}}',
  'hash' => '{{HASH}}',
  'beta' => '{{BETA}}',
  'analytics' => '{{ANALYTICS}}'
];

