const gulp = require('gulp');
const gutil = require('gulp-util');
const cond = require('gulp-cond');
const del = require('del');
const {argv} = require('yargs');
const runSequence = require('run-sequence');

const merge = require('merge-stream');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');

const eslint = require('gulp-eslint');

const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const mqpacker = require('css-mqpacker');
const cssnano = require('cssnano');

const browserSync = require('browser-sync').create();
const reload = browserSync.reload;

const watchify = require('watchify');
const browserify = require('browserify');
const hmr = require('browserify-hmr');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const replace = require('gulp-replace');
const childprocess = require('child_process');

const cache = require('gulp-cached');
const remember = require('gulp-remember');

// If gulp was called in the terminal with the --prod flag, set the node environment to production
if (argv.prod) {
  process.env.NODE_ENV = 'production';
}
// If gulp was called in the terminal with the --build flag, do not start server
if (argv.build) {
  process.env.BUILD = 'build';
}
const PROD = process.env.NODE_ENV === 'production';
const BUILD = process.env.BUILD === 'build';
const HASH = childprocess.execSync('git rev-parse --short HEAD').toString().replace(/^\s+|\s+$/g, '');
const BRANCH = childprocess.execSync('git rev-parse --abbrev-ref HEAD').toString().replace(/^\s+|\s+$/g, '');

console.log('branch:', BRANCH);
console.log('commit:', HASH);

// Configuration
const src = 'src';
const config = {
  port: PROD ? 443 : 3010,
  title: PROD ? 'terma invoices v1' : 'terma invoices v1 BETA',
  appUrl: PROD ? 'http://invoices.termaheat.pl' : 'http://invoices.termaheat.pl',
  analytics: PROD ? 'UA-11429636-25' : '',
  branch: BRANCH,
  hash: HASH,
  paths: {
    baseDir: 'public',
    js: src + '/js/**/*.js',
    css: src + '/scss/**/*.scss',
    index: 'public/index.php',
    env: src + '/env.php',
  }
};

/**
 * Gulp Tasks
 **/

// clears the contents of the dist and build folder
gulp.task('clean', function () {
  return del(['public/css/*', 'public/js/*']);
});

// linting
gulp.task('lint', function () {
  return gulp.src(config.paths.js)
    .pipe(eslint({
      fix: true
    }))
    .pipe(eslint.format())
});

// sass
gulp.task('css', function () {
  let processors = [
    autoprefixer({
      browsers: ['last 2 versions', '> 5%', 'Firefox ESR', 'Safari >= 8'],
      cascade: false
    }),
    mqpacker({
      sort: true
    }),
    cssnano
  ];

  return gulp.src(config.paths.css)
    .pipe(cache('sassing'))
    .pipe(sass({
      includePaths: 'node_modules/foundation-sites/scss',
    }).on('error', sass.logError))
    .pipe(postcss(processors))
    .pipe(cond(!PROD, sourcemaps.init({loadMaps: true})))
    .pipe(cond(!PROD, sourcemaps.write('./')))
    .pipe(remember('sassing'))
    .pipe(gulp.dest(config.paths.baseDir + '/css'));

});

// bundles our JS using browserify. Sourcemaps are used in development, while minification is used in production.
gulp.task('js', function () {
  gulp
  .src(src + '/js/testData.js')
  .pipe(gulp.dest(config.paths.baseDir + '/js'));

  let b = browserify({
    entries: src + '/js/main.js',
    debug: true,
    plugin: BUILD ? [] : [watchify],
    cache: {},
    packageCache: {},
    bundleExternal: true
  });
  return b.bundle()
    .on('error', function (err) {
      // print the error
      console.log(err.message);
      // end this stream
      this.emit('end');
    })
    .pipe(source('js/main.js'))
    .pipe(buffer())
    .pipe(cond(PROD, uglify()))
    .on('error', function (err) {
      // print the error
      console.log(err.message);
      // end this stream
      this.emit('end');
    })
    .pipe(cond(!PROD, sourcemaps.init({loadMaps: true})))
    .pipe(cond(!PROD, sourcemaps.write('./')))
    .pipe(gulp.dest(config.paths.baseDir));
});

// copy files
gulp.task('copy', function () {
    gulp
      .src(src + '/htaccess/' + (PROD ? 'prod' : 'beta') + '/.htaccess')
      .pipe(gulp.dest(config.paths.baseDir));
  }
);

// replace env strings upon build
gulp.task('replace', function () {
  gulp
    .src(config.paths.env)
    .pipe(replace('{{TITLE}}', config.title))
    .pipe(replace('{{APP_URL}}', config.appUrl))
    .pipe(replace('{{BRANCH}}', config.branch))
    .pipe(replace('{{HASH}}', config.hash))
    .pipe(replace('\'{{BETA}}\'', !PROD))
    .pipe(replace('{{ANALYTICS}}', config.analytics))
    .pipe(gulp.dest('./'));
});

// create a task that ensures the `css` task is complete before reloading browsers
gulp.task('css-watch', ['css'], function (done) {
  browserSync.reload();
  done();
});

// create a task that ensures the `js` task is complete before reloading browsers
gulp.task('js-watch', ['js'], function (done) {
  browserSync.reload();
  done();
});

// create a task that ensures the `replace` task is complete before reloading browsers
gulp.task('replace-watch', ['replace'], function (done) {
  browserSync.reload();
  done();
});

// watch for change & reload
gulp.task('watch', function () {
  browserSync.init({
    proxy: 'invoices.local',
    port: config.port
  });
  gulp.watch(config.paths.css, ['css-watch']);
  gulp.watch(config.paths.js, ['js-watch']);
  gulp.watch(config.paths.env, ['replace-watch']);
  gulp.watch(config.paths.index).on('change', browserSync.reload);
});

// default task, bundles the entire app and hosts it on an Express server
gulp.task('default', function (cb) {
  if (BUILD) {
    runSequence('clean', 'lint', 'css', 'js', 'copy', 'replace');
  } else {
    runSequence('clean', 'lint', 'css', 'js', 'copy', 'replace', 'watch', cb);
  }
});
